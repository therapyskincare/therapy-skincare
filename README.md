We are a results driven salon with extremely high standards that allows clients to leave our salon feeling like they have taken away the full benefit of every single treatment that they have with us.

Address: Lindrick Grove, Bradshaw, Halifax HX2 9QQ, United Kingdom

Phone: +44 1422 357572

Website: https://www.therapyskincare.co.uk
